var path = require('path');
module.exports = function(grunt) {
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.initConfig({
      helpers:          '<%=paths.build%>/dust-helpers.js',
      helpersMin:       '<%=paths.build%>/dust-helpers.min.js',
    
    less: {
      dist: {
        files: {
          'css/omnistore_checkout.css': 'less/omnistore_checkout.less',
          'css/ltc_checkout.css': 'less/ltc_checkout.less'
        }
      }
    },

    watch: {
      styles: {
        files: ['less/**/*.less','less/_partials/**/*.less'],
        tasks: ['less:dist'],
        options: {
         livereload: true,
         spawn: false
      }
    }


    }
  });
  grunt.registerTask('default', ['less:dist','watch']);  
}